<?php

require __DIR__ . '/vendor/autoload.php';


use Chat\Chat;
use Chat\ChatHandler;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

    try{
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new Chat(new ChatHandler())
                )
            ),
            \Chat\Config::$port
        );
        $server->run();
    } catch (\Exception $e){

    }