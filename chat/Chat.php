<?php

namespace Chat;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use \Exception;

/**
 * Получаем сообщение
 * Отправляем событие о получении
 * Получаем событи о прочтении
 * Отправляем событие о прочтении
 *
 * Тестовое: добавить свой токен
 */

/**
{
    event: "auth",
    data: {
        token: "wewrrwrwrwr"
        }
}
 *
 *
 * {
event: "send_message",
data: {
message: "wewrrwrwrwr",
 image: ""
}
}
 *
 *
 * {
event: "read_message",
data: {
token: "wewrrwrwrwr"
}
}
 */
class Chat implements MessageComponentInterface
{
    protected $clients;

    private $events;

    public function __construct(ChatEvents $events) {
        $this->events = $events;
        $this->clients = [];
    }

    public function onOpen(ConnectionInterface $conn) {
        echo "New connection! ({$conn->resourceId})\n";
        // Store the new connection to send messages to later
        $this->clients[$conn->resourceId] = $conn;
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        echo "Connection {$from->resourceId} onMessage\n";

        echo "Message {$msg} \n";

        try{
            $obj = json_decode($msg);
            switch ($obj->event){
                case "auth":
                    echo "Connection {$from->resourceId} auth\n";
                    $this->events->onAuthClient($from, $obj->data);
                    break;
                case "send_message":
                    echo "Connection {$from->resourceId} send_message\n";
                    $this->events->onSendMessage($from, $obj->data, $this->clients);
                    break;
                case "view_message":
                    echo "Connection {$from->resourceId} view_message\n";
                    $this->events->onViewMessage($from, $obj->data, $this->clients);
                    break;
                default:
                    echo "Error. Event not found!";
                    if(isset($this->clients[$from->resourceId])){
                        $this->clients[$from->resourceId]->send(json_encode([
                            "event" => "error",
                            "data" => [
                                "message" => "Event not found"
                            ]
                        ]));
                    }
            }
        } catch (Exception $e){
            echo $e->getMessage();
        }


        //$from->resourceId;

//        foreach ($this->clients as $client) {
//            if ($from !== $client) {
//                // The sender is not the receiver, send to each client connected
//                $client->send($msg);
//            }
//        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        echo "Connection {$conn->resourceId} has disconnected\n";
        $this->events->onDisconnectClient($conn);
        unset($this->clients[$conn->resourceId]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $this->events->onDisconnectClient($conn);
        unset($this->clients[$conn->resourceId]);
        $conn->close();
    }
}