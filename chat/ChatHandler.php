<?php
/**
 * Обработчик событий в чате
 */

namespace Chat;


use Ratchet\ConnectionInterface;

class ChatHandler extends ChatEvents
{

    /**
     * Подключение приложения к соккету
     * @return mixed
     */
    public function onConnectClient(ConnectionInterface $from)
    {
        $from->send(json_encode([
            "event" => "hello",
            "data" => [
                "message" => "Connect success"
            ]
        ]));
    }

    /**
     * Отключение приложения от соккета
     * @return mixed
     */
    public function onDisconnectClient(ConnectionInterface $from)
    {
        ConnectModel::removeConnection($from->resourceId);
    }

    /**
     * Авторизация клиента в соккете
     * @return mixed
     */
    public function onAuthClient(ConnectionInterface $from, $obj)
    {
        if(!isset($obj->token)){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Token not defined"
                ]
            ]));
            return;
        }

        /**
         * Ищем юзера
         */
        $user = UserModel::findByToken($obj->token);
        if(!$user){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Token not found"
                ]
            ]));
            return;
        }
        ConnectModel::addConnection($from->resourceId, $user->id);
        $from->send(json_encode([
            "event" => "auth",
            "data" => [
                "message" => "Auth OK"
            ]
        ]));
    }

    /**
     * Отправка сообщения клиентов в соккете
     * @return mixed
     */
    public function onSendMessage(ConnectionInterface $from, $obj, $clients)
    {
        if(!isset($obj->uid) || (!isset($obj->text) && !isset($obj->image)) || !isset($obj->recipient)){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Invalid format for message"
                ]
            ]));
            return;
        }
        /**
         * Ищем коннект отправителя
         */
        $conn_sender = ConnectModel::findBySocketId($from->resourceId);
        if(!$conn_sender){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Socket not found"
                ]
            ]));
            return;
        }
        /**
         * Ищем профиль получателя
         */
        $recipient = UserModel::findById($obj->recipient);
        if(!$recipient){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Recipient not found"
                ]
            ]));
            return;
        }
        /**
         * Сохраняем сообщение
         */
        if(!isset($obj->text)){
            $obj->text = "";
        }
        if(!isset($obj->image)){
            $obj->image = "";
        }
        MessageModel::addMessage($obj->uid, time()*1000, $obj->text, $obj->image, $conn->user_id, $obj->recipient);
        /**
         * Получаем сохранённое сообщение
         */
        $message = MessageModel::getMessageByUid($obj->uid);
        if(!$message){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Message not saved"
                ]
            ]));
            return;
        }

        /**
         * Отправляем опоненту
         */
        $recipient_conn = ConnectModel::findAllByUserId($recipient->id);
        foreach ($recipient_conn as $conn){
            $client = $this->getClient($clients, $conn->id);
            if($client){
                $client->send(json_encode([
                    "event" => "new_message",
                    "data" => [
                        "id" => $message->id,
                        "uid" => $message->uid,
                        "time" => $message->time,
                        "text" => $message->text,
                        "is_view" => $message->is_view,
                        "sender" => $message->sender,
                        "recipient" => $message->recipient
                    ]
                ]));
            }
        }

        /**
         * Отправляем получателю уведомление о доставке на сервер
         */
        $from->send(json_encode([
            "event" => "send_success",
            "data" => [
                "id" => $message->id,
                "uid" => $message->uid,
                "time" => $message->time,
                "text" => $message->text,
                "is_view" => $message->is_view,
                "sender" => $message->sender,
                "recipient" => $message->recipient
            ]
        ]));
    }

    /**
     * Просмотр сообщения клиентом в соккете
     * @return mixed
     */
    public function onViewMessage(ConnectionInterface $from, $obj, $clients)
    {
        if(!isset($obj->uid)){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Invalid format for message"
                ]
            ]));
            return;
        }
        /**
         * Ищем коннекты получателя
         */
        $conn_recipient = ConnectModel::findBySocketId($from->resourceId);
        if(!$conn_recipient){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Opponent offline"
                ]
            ]));
            return;
        }
        /**
         * Ищем сообщение
         */
        $message = MessageModel::getMessageByUid($obj->uid);
        if(!$message){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "Message not found"
                ]
            ]));
            return;
        }
        /**
         * Проверяем принадлежность сообщения
         */
        if($message->recipient != $conn_recipient->user_id){
            $from->send(json_encode([
                "event" => "error",
                "data" => [
                    "message" => "The recipient does not fit the profile"
                ]
            ]));
            return;
        }
        /**
         * Отмечаем сообщение прочитанным
         */
        MessageModel::setView($message->id);

        /**
         * Ищем коннекты отправителя
         */
        $conn_sender = ConnectModel::findAllByUserId($message->sender);
        /**
         * Отправляем событие отправителю сообщения
         */
        foreach ($conn_sender as $conn){
            $client = $this->getClient($clients, $conn->id);
            if($client){
                $client->send(json_encode([
                    "event" => "message_viewed",
                    "data" => [
                        "uid" => $message->uid
                    ]
                ]));
            }
        }
    }

    private function getClient($clients, $connection_id){
        if(isset($clients[$connection_id])){
            return $clients[$connection_id];
        } else {
            ConnectModel::removeConnection($connection_id);
            return false;
        }
    }
}