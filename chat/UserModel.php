<?php
/**
 * Пользователь
 */

namespace Chat;


class UserModel
{
    /**
     * Поиск пользователя по токену
     * @param $token
     * @return null|object|\stdClass
     */
    public static function findByToken($token){
        return ConnectionDb::queryRow("SELECT * FROM ".Config::$table_user." WHERE access_token LIKE '".$token."' LIMIT 1");
    }

    /**
     * Поиск пользователя id
     * @param $id
     * @return null|object|\stdClass
     */
    public static function findById($id){
        return ConnectionDb::queryRow("SELECT * FROM ".Config::$table_user." WHERE id = '".$id."' LIMIT 1");
    }
}