<?php
/**
 * Модель данных для соккета
 */

namespace Chat;

class SocketData
{
    public $event;
    public $data;
    public function getJson(){
        return json_encode([
            "event" => $this->event,
            "data" => $this->data
        ]);
    }
}