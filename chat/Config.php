<?php
/**
 * Кофигурация чата
 */

namespace Chat;


class Config
{
    /**
     * Подключение к бд
     * @var string
     */
    public static $db_host = "localhost";
    public static $db_user = "phpchat";
    public static $db_password = "phpchat";
    public static $db_name = "phpchat";

    /**
     * Порт для чата
     * @var int
     */
    public static $port = 8080;

    /**
     * Имена таблиц
     */
    public static $table_user = "users";
    public static $table_messages = "messages";
    public static $table_connects = "connects";
}