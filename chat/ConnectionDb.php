<?php
/**
 * Класс для запросов к бд
 */

namespace Chat;


class ConnectionDb
{
    private static $connection = null;

    public static function getConnection(){
        if(ConnectionDb::$connection == null){
            ConnectionDb::$connection = mysql_connect(Config::$db_host,Config::$db_user,Config::$db_password);
            mysql_set_charset('utf8', ConnectionDb::$connection);
            mysql_select_db(Config::$db_name);
            return ConnectionDb::$connection;
        } else {
            return ConnectionDb::$connection;
        }
    }

    public static function queryRow($sql){
        $query = mysql_query($sql, ConnectionDb::getConnection());
        if(!$query){
            return null;
        }
        return mysql_fetch_object($query);
    }

    public static function queryRows($sql){
        $result = [];
        $query = mysql_query($sql, ConnectionDb::getConnection());
        if(!$query){
            return $result;
        }
        while ($row =  mysql_fetch_object($query)){
            $result[] = $row;
        }
        return $result;
    }

    public static function query($sql){
        $query = mysql_query($sql, ConnectionDb::getConnection());
        if(!$query){
            return null;
        }
        return $query;
    }
}