<?php
/**
 * Модель подключений к соккету
 */

namespace chat;


class ConnectModel
{
    public static function findByUserId($user_id){
        return ConnectionDb::queryRow("SELECT * FROM ".Config::$table_connects." WHERE user_id =".$user_id." LIMIT 1");
    }

    public static function findAllByUserId($user_id){
        return ConnectionDb::queryRows("SELECT * FROM ".Config::$table_connects." WHERE user_id =".$user_id);
    }

    public static function findBySocketId($socket_id){
        return ConnectionDb::queryRow("SELECT * FROM ".Config::$table_connects." WHERE id =".$socket_id." LIMIT 1");
    }

    public static function addConnection($socket_id, $user_id){
        $connect = ConnectionDb::queryRow("SELECT * FROM ".Config::$table_connects." WHERE user_id =".$user_id." AND id=".$socket_id." LIMIT 1");
        if($connect == null){
            ConnectionDb::query("INSERT INTO ".Config::$table_connects." (id, user_id) VALUES(".$socket_id.",".$user_id.")");
        }
    }

    public static function removeConnection($socket_id){
        ConnectionDb::query("DELETE FROM ".Config::$table_connects." WHERE id=".$socket_id);
    }

    public static function removeAllConnection($user_id){
        ConnectionDb::query("DELETE FROM ".Config::$table_connects." WHERE user_id =".$user_id);
    }
}