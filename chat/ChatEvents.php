<?php
/**
 * Created by IntelliJ IDEA.
 * User: devapp
 * Date: 07/12/16
 * Time: 21:54
 */

namespace chat;


use Ratchet\ConnectionInterface;

abstract class ChatEvents
{
    /**
     * Подключение приложения к соккету
     * @return mixed
     */
    public abstract function onConnectClient(ConnectionInterface $from);

    /**
     * Отключение приложения от соккета
     * @return mixed
     */
    public abstract function onDisconnectClient(ConnectionInterface $from);

    /**
     * Авторизация клиента в соккете
     * @return mixed
     */
    public abstract function onAuthClient(ConnectionInterface $from, $obj);

    /**
     * Отправка сообщения клиентов в соккете
     * @return mixed
     */
    public abstract function onSendMessage(ConnectionInterface $from, $obj, $clients);

    /**
     * Просмотр сообщения клиентом в соккете
     * @return mixed
     */
    public abstract function onViewMessage(ConnectionInterface $from, $obj, $clients);
}